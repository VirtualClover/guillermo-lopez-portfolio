module.exports = {
  siteMetadata: {
    title: "guillermo-lopez-portfolio",
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-mdx",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `VirtualCloverPortfolio`,
        short_name: `VCPortfolio`,
        start_url: `/`,
        background_color: `#F5F5EF`,
        theme_color: `#F5F5EF`,
        display: `standalone`,
        icon: `src/images/general/favicon.png`,
      },
    },
  ],
};
