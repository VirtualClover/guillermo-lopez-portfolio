import * as styles from './index.module.scss';

import React, { useState } from 'react';
import { companies, personalData } from '../utils/data/globalData';
import {
  faArrowLeft,
  faArrowRight,
  faBriefcase,
  faEnvelope,
  faGlasses,
  faPaintBrush,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

import { VCMainContainer } from '../globalComponents/VCMainContainer';
import { VCPortfolioList } from '../globalComponents/VCPortfolioList';
import { VCSurvey } from '../globalComponents/VCSurvey';
import { fab } from '@fortawesome/free-brands-svg-icons';
//Icons
import { library } from '@fortawesome/fontawesome-svg-core';
import { motion } from 'framer-motion';
import ogImage from '../images/ogImages/home.jpg';

library.add(
  fab,
  faGlasses,
  faEnvelope,
  faArrowRight,
  faArrowLeft,
  faPaintBrush,
  faUser,
  faBriefcase
);

// markup
const IndexPage = () => {
  return (
    <VCMainContainer
      meta={{
        title: personalData.name,
        description: 'This is the portafolio page of Guillermo López.',
        ogType: 'website',
        ogImage: ogImage,
      }}
      navConfig={{
        buttons: [
          {
            link: '/about-me',
            icon: 'user',
            label: 'Got to the about me section.',
          },
        ],
        title: 'fadeIn',
      }}
    >
      <div className={styles.mainWrapper}>
        <motion.section
          initial={{ x: -1920 }}
          animate={{ x: 0 }}
          exit={{ x: -1920 }}
          transition={{
            type: 'spring',
            mass: 0.35,
            stiffness: 75,
            duration: 0.4,
          }}
          className={styles.firstHalf}
          vocab="http://schema.org/"
          prefix="ov: http://open.vocab.org/terms/"
          resource="#manu"
          typeof="Person"
        >
          <h1 property="name" aria-label={`${personalData.name}.`}>
            {personalData.name.split('').map(function (letter, i) {
              return letter != ' ' ? (
                <motion.span
                  key={i}
                  initial={{
                    y: 300,
                    opacity: 0,
                    color: `hsl(${244 - i * 10}, 100%, 50%)`,
                  }}
                  animate={{ y: 0, opacity: 1, color: '#000' }}
                  transition={{
                    type: 'spring',
                    mass: 0.9,
                    delay: 0.4 + 0.02 * i,
                    stiffness: 75,
                    duration: 0.4,
                  }}
                >
                  {letter}
                </motion.span>
              ) : (
                <span>&nbsp;</span>
              );
            })}
          </h1>
          <p aria-label={`Product designer / Frontend developer.`}>
            Product designer / Frontend developer
          </p>
        </motion.section>
        <section>
          <VCPortfolioList />
        </section>
      </div>
    </VCMainContainer>
  );
};

export default IndexPage;
