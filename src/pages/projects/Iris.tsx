import { Link } from 'gatsby';
import React from 'react';
import { VCArticle } from '../../globalComponents/VCArticle/VCArticle';
import { VCSectionBlock } from '../../globalComponents/VCSectionBlock';
import challengeImage from '../../images/projects/Iris/challenge.svg';
import componentsImage from '../../images/projects/Iris/components.jpg';
import componentsImagew from '../../images/projects/Iris/components.webp';
import foundationsImage from '../../images/projects/Iris/foundations.svg';
import illustrations from '../../images/projects/Iris/illustrations.svg';
import { listItems } from '../../utils/data/globalData';
import ogImage from '../../images/ogImages/iris.jpg';

// markup
const ProjectPage = () => {
  const projectData = listItems[2];
  const totalBlocks = 5;

  return (
    <VCArticle metaData={projectData} ogImage={ogImage}>
      <VCSectionBlock
        title="The challenge"
        sectionNumber={1}
        sectionTotal={totalBlocks}
        image={{
          jpg: challengeImage,
          webp: challengeImage,
          alt: `A lo-fi concept of what My Store's look should be`,
        }}
      >
        <p>
          <a
            href="https://www.middletownpress.com/business/article/Do-you-want-to-grow-your-online-sales-16641131.php"
            target={'_blank'}
          >
            {' '}
            My Store was Canasta Rosa’s sibling service.{' '}
          </a>
          <br /> It was created to serve as a robust hub where you could connect
          to all of Mexico’s major e-commerce platforms. The idea was to create
          a service that would share similar UI characteristics with{' '}
          <b> Canasta Rosa but with a fresh, cleaner look.</b>
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="From MIJO to Iris"
        sectionNumber={2}
        sectionTotal={totalBlocks}
        image={{
          jpg: foundationsImage,
          webp: foundationsImage,
          alt: `Applying the 4-shade structure from MIJO to Iris' color palette.`,
        }}
      >
        <p>
          We decided to make things right by creating the Design System in
          parallel to the creation of the MVP of the service. <br />
          <br /> Thankfully, since the look should be similar to that of
          Canasta’s,{' '}
          <b>
            we reuse the same structure as <Link to="/projects/MIJO">MIJO</Link>{' '}
            (Canasta’s Design System) but with a purple spin to it.
          </b>
          <br />
          <br />
          Fun fact, the Design System is called Iris, because of My Store's{' '}
          <a
            href="https://en.wikipedia.org/wiki/Iris_(color)"
            target={'_blank'}
          >
            purple color
          </a>{' '}
          and because is also an acronym for{' '}
          <b>(Incredible Reusable Interface System).</b>
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="Updating old components"
        sectionNumber={3}
        sectionTotal={totalBlocks}
        image={{
          jpg: componentsImage,
          webp: componentsImagew,
          alt: `Re-coloring MIJO's components.`,
        }}
      >
        <p>
          I mainly did a color swap to our existing components and started
          applying auto-layout to some(MIJO was created pre auto-layout). <br />{' '}
          <br />
          This allowed me to focus on more robust components like{' '}
          <a
            href="https://www.figma.com/file/vANdzsGpqlhXhlFdHAJioP/IRIS-Master?node-id=864%3A12116&t=YjkGBtn6llzqIxIq-1"
            target={'_blank'}
          >
            {' '}
            forms
          </a>
          .
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="Illustrations"
        sectionNumber={4}
        sectionTotal={totalBlocks}
        image={{
          jpg: illustrations,
          webp: illustrations,
          alt: `Iris' illustrations.`,
        }}
      >
        <p>
          To keep with the cleaner look,{' '}
          <a
            href="https://www.figma.com/file/vANdzsGpqlhXhlFdHAJioP/IRIS-Master?node-id=840%3A0&t=YjkGBtn6llzqIxIq-1"
            target={'_blank'}
          >
            {' '}
            Iris' illustrations
          </a>{' '}
          were super <b>simple, and flat and used mainly pastel colors</b>, this
          allowed the team to create them really easily.
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="The end result"
        sectionNumber={5}
        sectionTotal={totalBlocks}
        iFrame={{
          src: 'https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FvANdzsGpqlhXhlFdHAJioP%2FIRIS-Master%3Fnode-id%3D2001%253A6424%26t%3DYjkGBtn6llzqIxIq-1',
          alt: `Iris' library`,
        }}
      >
        <p>
          Given that we needed to launch My Store’s MVP as quickly as possible,
          Iris was made in a very small timeframe(1 1/2 weeks), but, since I
          already had the experience of creating Mijo and used it as a
          blueprint,{' '}
          <b>
            it allowed me to add a lot of many other molecules and template
            components that allowed use to created landing pages and dashboards
            incredible quick.
          </b>
        </p>
      </VCSectionBlock>
    </VCArticle>
  );
};

export default ProjectPage;
