import React from 'react';
import { VCArticle } from '../../globalComponents/VCArticle/VCArticle';
import { VCSectionBlock } from '../../globalComponents/VCSectionBlock';
import challengeImage from '../../images/projects/MIJO/challenge.jpg';
import challengeImagew from '../../images/projects/MIJO/challenge.svg';
import componentsImage from '../../images/projects/MIJO/components.jpg';
import componentsImagew from '../../images/projects/MIJO/components.svg';
import foundationsImage from '../../images/projects/MIJO/foundations.svg';
import { listItems } from '../../utils/data/globalData';
import ogImage from '../../images/ogImages/mijo.jpg';
import workflowImage from '../../images/projects/MIJO/workflow.jpg';
import workflowImagew from '../../images/projects/MIJO/workflow.webp';

// markup
const ProjectPage = () => {
  const projectData = listItems[1];
  const totalBlocks = 5;

  return (
    <VCArticle metaData={projectData} ogImage={ogImage}>
      <VCSectionBlock
        title="The challenge"
        sectionNumber={1}
        sectionTotal={totalBlocks}
        image={{
          jpg: challengeImage,
          webp: challengeImagew,
          alt: `AN EXAMPLE OF CANASTA ROSA'S UI.`,
        }}
      >
        <a
          href="https://www.forbes.com.mx/negocios-canasta-rosa-emprendedores-comercio-electronico-coronavirus/"
          target="_blank"
        >
          Canasta Rosa was an artisan e-commerce.
        </a>
        <br />
        <p>
          As it became a household name for e-commerce in Mexico, we needed to
          expand from only having a site to also having a mobile app, but, since
          the former one was developed by an external agency, it offered a
          completely different look and experience to the users than the
          website. <br /> <br />
          So we needed to{' '}
          <b>
            unify the image and experience of a platform that would boost the
            reach and voice of small businesses during the pandemic.
          </b>
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="The basics"
        sectionNumber={2}
        sectionTotal={totalBlocks}
        image={{
          jpg: foundationsImage,
          webp: foundationsImage,
          alt: `MIJO's foundations.`,
        }}
      >
        <p>
          The first thing we needed to do was unify our first atoms: colors,
          text styles, and icons.
        </p>
        <ul>
          <li>
            <a
              href="https://www.figma.com/file/HP4IPEcsEzBt9O6fVzXL3U/MIJO-Master?node-id=3%3A197&t=sxIHUqotDM2rugSn-1"
              target={'_blank'}
            >
              For colors
            </a>
            , I took every color we were using in both platforms and grouped
            them in shades.
          </li>
          <li>
            <a
              href="https://www.figma.com/file/HP4IPEcsEzBt9O6fVzXL3U/MIJO-Master?node-id=1%3A2&t=sxIHUqotDM2rugSn-1"
              target={'_blank'}
            >
              For text styles{' '}
            </a>
            , I started to create a hierarchy based on the text styles we had in
            production with some tweaks.{' '}
          </li>
          <li>
            <a href="https://www.figma.com/file/HP4IPEcsEzBt9O6fVzXL3U/MIJO-Master?node-id=99%3A126&t=sxIHUqotDM2rugSn-1">
              For icons
            </a>
            , we try to use a very rounded style to make the icons more
            friendly.
          </li>
        </ul>
      </VCSectionBlock>
      <VCSectionBlock
        title="The components"
        sectionNumber={3}
        sectionTotal={totalBlocks}
        image={{
          jpg: componentsImage,
          webp: componentsImagew,
          alt: `Some components from MIJO.`,
        }}
      >
        <p>
          Once we created the basic building blocks of our UI, we needed to
          create new components from scratch.
          <br /> <br />I would often spend some time of my sprint doing so, from
          simple{' '}
          <a
            href="https://www.figma.com/file/HP4IPEcsEzBt9O6fVzXL3U/MIJO-Master?node-id=299%3A536&t=sxIHUqotDM2rugSn-1"
            target={'_blank'}
          >
            inputs like buttons
          </a>
          , to complex ones like{' '}
          <a
            href="https://www.figma.com/file/HP4IPEcsEzBt9O6fVzXL3U/MIJO-Master?node-id=299%3A540&t=sxIHUqotDM2rugSn-1"
            target={'_blank'}
          >
            navigation bars
          </a>
          , but as time passed and the system grew, other designers and devs
          started adding their own components.
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="Unifying our workflow"
        sectionNumber={4}
        sectionTotal={totalBlocks}
        image={{
          jpg: workflowImage,
          webp: workflowImagew,
          alt: `The workflow of adding a component to the design system.`,
        }}
      >
        <p>
          Once the DS started to gain traction, I optimized the workflow of
          adding things to the Design System via Sketch plugins like{' '}
          <a href="https://github.com/airbnb/react-sketchapp" target="_blank">
            AirBnB's react-sketchapp
          </a>{' '}
          for using the{' '}
          <b>same coded component in Sketch, Figma, React and React native.</b>
          <br />
          <br />
          And, once every component was unified, I deployed a
          <a
            href="https://virtualclover.gitlab.io/mijo/master/"
            target={'_blank'}
          >
            {' '}
            Storybook site
          </a>{' '}
          to work as our single source of truth.
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="The end result"
        sectionNumber={5}
        sectionTotal={totalBlocks}
        iFrame={{
          src: 'https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FHP4IPEcsEzBt9O6fVzXL3U%2FMIJO-Master%3Fnode-id%3D801%253A3616%26t%3Dmu2MVc1qtw44w6o8-1',
          alt: `Mijo's library`,
        }}
      >
        <p>
          With an unified workflow, every component that we created was released
          on every platform at once. And we could create and{' '}
          <b>redesign features super easily and fast</b>, since designers and
          devs were, in a way, speaking the same language.
        </p>
      </VCSectionBlock>
    </VCArticle>
  );
};

export default ProjectPage;
