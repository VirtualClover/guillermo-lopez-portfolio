import React from 'react';
import { VCArticle } from '../../globalComponents/VCArticle/VCArticle';
import { VCSectionBlock } from '../../globalComponents/VCSectionBlock';
import challengeImage from '../../images/projects/Flare/challenge.svg';
import components from '../../images/projects/Flare/components.jpg';
import componentsw from '../../images/projects/Flare/components.webp';
import foundations from '../../images/projects/Flare/basics.svg';
import galileo from '../../images/projects/Flare/Galileo.mp4';
import galileow from '../../images/projects/Flare/Galileo.webm';
import illusts from '../../images/projects/Flare/illusts.mp4';
import illustsw from '../../images/projects/Flare/illusts.webm';
import inspector from '../../images/projects/Flare/Flare.mp4';
import inspectorw from '../../images/projects/Flare/Flare.webm';
import { listItems } from '../../utils/data/globalData';
import ogImage from '../../images/ogImages/flare.jpg';

// markup
const ProjectPage = () => {
  const projectData = listItems[0];
  const totalBlocks = 7;

  return (
    <VCArticle metaData={projectData} ogImage={ogImage}>
      <VCSectionBlock
        title="The challenge"
        sectionNumber={1}
        sectionTotal={totalBlocks}
        image={{
          jpg: challengeImage,
          webp: challengeImage,
          alt: `Bitso's widly different UI for the same feature.`,
        }}
      >
        <p>
          Ever since{' '}
          <a href="https://www.bitso.com/" target="_blank">
            Bitso
          </a>{' '}
          became an international brand there was a need to improve the
          <b>
            {' '}
            visual, content, and brand consistency across products and
            platforms.
          </b>
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="The basics"
        sectionNumber={2}
        sectionTotal={totalBlocks}
        image={{
          jpg: foundations,
          webp: foundations,
          alt: `Flare's foundations.`,
        }}
      >
        <p>
          The first thing we needed to do was establish our Design system
          foundations.
          <br />{' '}
          <a
            href="https://www.figma.com/file/tME6tH8PqFoRQZe1gd16hG/Flare-Public?node-id=1%3A17598&t=ks41PUSkgQHFsREw-1"
            target={'_blank'}
          >
            These foundations
          </a>{' '}
          represent Bitso's Design Language through basic guidelines for
          designers and developers to make informed decisions about style for
          creating anything, from a button to a full page that are consistent
          with each other. <br />
          <br />
          One highlight from this is that,{' '}
          <b>
            for colors, we organized them into{' '}
            <a
              href="https://www.figma.com/file/tME6tH8PqFoRQZe1gd16hG/Flare-Public?node-id=1%3A31258&t=DDkVfEFq020FSaiW-1"
              target={'_blank'}
            >
              theme-like structures{' '}
            </a>{' '}
            so you could change between light and dark modes really quickly on
            every platform.
          </b>
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="The components"
        sectionNumber={3}
        sectionTotal={totalBlocks}
        image={{
          jpg: components,
          webp: componentsw,
          alt: `FLARE'S COMPONENTS.`,
        }}
      >
        <p>
          Once the foundations were established, we started creating
          <a
            href="https://www.figma.com/file/tME6tH8PqFoRQZe1gd16hG/Flare-Public?node-id=1%3A34336&t=ks41PUSkgQHFsREw-1"
            target={'_blank'}
          >
            {' '}
            components{' '}
          </a>{' '}
          for the mobile app and for the website — each one following the best
          practices in terms of accessibility and UX.
          <br />
          <br />
          Thanks to our{' '}
          <a
            href="https://www.figma.com/file/tME6tH8PqFoRQZe1gd16hG/Flare-Public?node-id=1%3A31258&t=DDkVfEFq020FSaiW-1"
            target={'_blank'}
          >
            theme structure
          </a>
          , these components can function in dark mode out of the box with no
          issues.
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="Illustrations"
        sectionNumber={4}
        sectionTotal={totalBlocks}
        video={{
          loop: true,
          mp4: illusts,
          webm: illustsw,
          alt: `Some illustrations from Flare animated.`,
        }}
      >
        <p>
          For{' '}
          <a
            href={
              'https://www.figma.com/file/tME6tH8PqFoRQZe1gd16hG/Flare-Public?node-id=1%3A67727&t=AgmL1IjtQWz8c215-1'
            }
            target={'_blank'}
          >
            the illustrations
          </a>
          , we decided to go with simple shapes so that characters could be
          created and animated easily with a color palette that originated from
          our main brand colors.
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="Flare inspector"
        sectionNumber={5}
        sectionTotal={totalBlocks}
        video={{
          mp4: inspector,
          webm: inspectorw,
          alt: `Showcasing the Flare inspector plugin to the team.`,
        }}
      >
        <p>
          The Flare inspector is a <b>Figma plugin</b> I created to increase the
          adoption of the design systems on other squads,{' '}
          <b>
            it can tell you the things that do not align with the Design System
            guidelines in your specific design and how to correct them.{' '}
          </b>
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="Galileo"
        sectionNumber={6}
        sectionTotal={totalBlocks}
        video={{
          mp4: galileo,
          webm: galileow,
          alt: `A showcase of Galileo.`,
        }}
      >
        <p>
          Maintaining a Design system is not easy, so I also{' '}
          <a
            href="https://github.com/VirtualClover/Galileo-API"
            target={'_blank'}
          >
            created a service
          </a>{' '}
          that uses data in a spreadsheet to{' '}
          <b>automatically update our code base, and our documentation</b>; it
          also connects to a{' '}
          <a
            href="https://gitlab.com/VirtualClover/galileo-figma-plugin"
            target={'_blank'}
          >
            Figma plugin
          </a>{' '}
          so you can update libraries really easily.
        </p>
      </VCSectionBlock>
      <VCSectionBlock
        title="Final thoughts"
        sectionNumber={7}
        sectionTotal={totalBlocks}
        iFrame={{
          src: 'https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FtME6tH8PqFoRQZe1gd16hG%2FFlare-Public%3Fnode-id%3D1%253A3%26t%3DcbH4IkKxC4hl8y1T-1',
          alt: `Flare design system.`,
        }}
      >
        <p>
          Flare was an incredible challenge about <b>scaling a Design System</b>{' '}
          to serve different teams on different platforms, each one with it's
          own little identity that branches from the main brand but that feels
          as part of the same family.
        </p>
      </VCSectionBlock>
    </VCArticle>
  );
};

export default ProjectPage;
