import React from 'react';
import { VCArticle } from '../../globalComponents/VCArticle/VCArticle';
import { VCSectionBlock } from '../../globalComponents/VCSectionBlock';
import challengeImage from '../../images/projects/RX/challenge.jpg';
import challengeImagew from '../../images/projects/RX/challenge.webp';
import { listItems } from '../../utils/data/globalData';
import ogImage from '../../images/ogImages/mijo.jpg';

// markup
const ProjectPage = () => {
  const projectData = listItems[3];
  const totalBlocks = 1;

  return (
    <VCArticle metaData={projectData} ogImage={ogImage}>
      <VCSectionBlock
        title="The site"
        sectionNumber={1}
        sectionTotal={totalBlocks}
        image={{
          jpg: challengeImage,
          webp: challengeImagew,
          alt: `The first fold of the page.`,
        }}
      >
        <p>
          RX Watches was{' '}
          <b> a demo for a shopping site that I was working on</b>. The focus
          was a <b>minimalist menu and navigation </b>, while also having a
          pop-up design meaning that the site plays a lot with the depth of the
          elements going behind and on top of others without interrupting the
          flow of the experience. <br />
          <br />
          The webpage does not have any sort of backend service implementation
          so pages like the checkout are just static. The project is open source
          for everyone that wants to use it,{' '}
          <b>it is developed in vanilla HTML, CSS, and JS.</b>
        </p>
        <br />
        <a
          href="https://virtualclover.com.mx/web/RX/hosted/index.html"
          target="_blank"
        >
          Check the coded site!
        </a>
      </VCSectionBlock>
    </VCArticle>
  );
};

export default ProjectPage;
