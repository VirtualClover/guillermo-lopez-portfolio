import * as styles from './about-me.module.scss';

import {
  faArrowLeft,
  faArrowRight,
  faBriefcase,
  faEnvelope,
  faFileDownload,
  faGlasses,
  faPaintBrush,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

import React from 'react';
import { VCLink } from '../globalComponents/VCLink';
import { VCMainContainer } from '../globalComponents/VCMainContainer';
import { VCTimeline } from '../globalComponents/VCTimeLine';
import { fab } from '@fortawesome/free-brands-svg-icons';
//Icons
import { library } from '@fortawesome/fontawesome-svg-core';
import { motion } from 'framer-motion';
import ogImage from '../images/ogImages/home.jpg';
import { personalData } from '../utils/data/globalData';

library.add(
  fab,
  faGlasses,
  faEnvelope,
  faArrowRight,
  faArrowLeft,
  faPaintBrush,
  faUser,
  faBriefcase,
  faFileDownload
);

// markup
const IndexPage = () => {
  return (
    <VCMainContainer
      meta={{
        title: `${personalData.name} | About me`,
        description: 'About me',
        ogType: 'profile',
        ogImage,
      }}
      navConfig={{
        buttons: [
          { link: '/', icon: 'briefcase', label: 'Go to my portfolio.' },
        ],
        title: 'none',
      }}
      footerTouchPoints={false}
    >
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{
          type: 'spring',
          delay: 0.1,
          duration: 0.5,
        }}
        className={styles.mainWrapper}
      >
        <motion.section
          initial={{ x: -1920 }}
          animate={{ x: 0 }}
          exit={{ x: -1920 }}
          transition={{
            type: 'spring',
            mass: 0.35,
            stiffness: 75,
            duration: 0.4,
          }}
          className={styles.firstHalf}
        >
          <h1 aria-label={`${personalData.name}.`}>{personalData.name}</h1>
          <p>
            Product designer / Frontend developer that loves{' '}
            <b>
              Design Systems and <abbr title="Proof of concept">POC</abbr>
            </b>{' '}
            projects.
          </p>
          <ul>
            <li>
              <VCLink
                icon={'envelope'}
                label={'Send me a mail!'}
                link={'mailto:memo@virtualclover.ltd'}
              />
            </li>
            <li>
              <VCLink
                icon={['fab', 'github']}
                label={'Check my git repos.'}
                link={'https://gitlab.com/VirtualClover'}
              />
            </li>
            <li>
              <VCLink
                icon={['fab', 'linkedin']}
                label={'Go to my linkedIn profile.'}
                link={'https://www.linkedin.com/in/vc-memo-lopez/'}
              />
            </li>
            <li>
              <VCLink
                icon={'paint-brush'}
                label={'Got to my illustration portfolio.'}
                link={'https://virtualclover.tumblr.com/'}
              />
            </li>
            <li>
              <VCLink
                link="files/CV.pdf"
                icon="file-download"
                label="Download my resume."
              />
            </li>
          </ul>
        </motion.section>
        <section className={styles.timeline}>
          <VCTimeline />
        </section>
      </motion.div>
    </VCMainContainer>
  );
};

export default IndexPage;
