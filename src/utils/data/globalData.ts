import { Company, Job, Project } from '../types/globalTypes';

import amazonImage from '../../images/companies/amazon.jpg';
import bitso from '../../images/resume/bitso.jpg';
import bitsow from '../../images/resume/bitso.webp';
import canasta from '../../images/resume/canasta.jpg';
import canastaw from '../../images/resume/canasta.webp';
import discordImage from '../../images/companies/discord.jpg';
import duckduckgoImage from '../../images/companies/duckduckgo.jpg';
import flare1 from '../../images/projects/Flare/preview/1.svg';
import flare2 from '../../images/projects/Flare/preview/2.jpg';
import flare2w from '../../images/projects/Flare/preview/2.svg';
import flare3 from '../../images/projects/Flare/preview/3.jpg';
import flare3w from '../../images/projects/Flare/preview/3.webp';
import gbmImage from '../../images/companies/gbm.jpg';
import gitlabImage from '../../images/companies/gitlab.jpg';
import googleImage from '../../images/companies/google.jpg';
import ibmImage from '../../images/companies/ibm.jpg';
import illusts1 from '../../images/projects/Illusts/preview/1.jpg';
import illusts1w from '../../images/projects/Illusts/preview/1.webp';
import illusts2 from '../../images/projects/Illusts/preview/2.jpg';
import illusts2w from '../../images/projects/Illusts/preview/2.webp';
import illusts3 from '../../images/projects/Illusts/preview/3.jpg';
import illusts3w from '../../images/projects/Illusts/preview/3.webp';
import iris1 from '../../images/projects/Iris/preview/1.jpg';
import iris1w from '../../images/projects/Iris/preview/1.webp';
import iris2 from '../../images/projects/Iris/preview/2.svg';
import iris3 from '../../images/projects/Iris/preview/3.svg';
import mercadoLibreImage from '../../images/companies/mercadoLibre.jpg';
import metaImage from '../../images/companies/meta.jpg';
import microsoftImage from '../../images/companies/microsoft.jpg';
import mijo1 from '../../images/projects/MIJO/preview/1.svg';
import mijo2 from '../../images/projects/MIJO/preview/2.jpg';
import mijo2w from '../../images/projects/MIJO/preview/2.webp';
import mijo3 from '../../images/projects/MIJO/preview/3.svg';
import paypalImage from '../../images/companies/paypal.jpg';
import rx1 from '../../images/projects/RX/preview/1.jpg';
import rx1w from '../../images/projects/RX/preview/1.webp';
import rx2 from '../../images/projects/RX/preview/2.jpg';
import rx2w from '../../images/projects/RX/preview/2.webp';
import rx3 from '../../images/projects/RX/preview/3.jpg';
import rx3w from '../../images/projects/RX/preview/3.webp';
import secoco from '../../images/resume/secoco.jpg';
import secocow from '../../images/resume/secoco.webp';
import spotifyImage from '../../images/companies/spotify.jpg';
import uberImage from '../../images/companies/uber.jpg';

export const listItems: Array<Project> = [
  {
    projectTitle: 'Flare',
    year: 2022,
    readingTime: 3,
    link: '/projects/Flare',
    figmaLink:
      'https://www.figma.com/file/tME6tH8PqFoRQZe1gd16hG/Flare-Public?node-id=1%3A3&t=5hRL9u4WUdJzg9ms-1',
    gitLink: 'https://github.com/VirtualClover/Moon-System',
    images: [
      { jpg: flare1, webp: flare1, alt: `FLARE's LOGO.` },
      { jpg: flare2, webp: flare2w, alt: `FLARE's ILLUSTRATIONS.` },
      {
        jpg: flare3,
        webp: flare3w,
        alt: `USING FLARE IN DIFFERENT SCREENS.`,
        main: true,
      },
    ],
    description: `Bitso's Design System.`,
  },
  {
    projectTitle: 'Mijo DS',
    year: 2021,
    readingTime: 2,
    link: '/projects/MIJO',
    figmaLink:
      'https://www.figma.com/file/HP4IPEcsEzBt9O6fVzXL3U/MIJO-Master?t=4CvOi2HhNZvfCPHo-6',
    gitLink: 'https://gitlab.com/VirtualClover/mijo',
    images: [
      { jpg: mijo1, webp: mijo1, alt: 'Mijo DS.' },
      { jpg: mijo2, webp: mijo2w, alt: `Mobile screens using MIJO.`, main: true },
      { jpg: mijo3, webp: mijo3, alt: `Mijo's icons.` },
    ],
    description: 'A Design System I created for Canasta Rosa.',
  },
  {
    projectTitle: 'Iris DS',
    year: 2021,
    readingTime: 3,
    link: '/projects/Iris',
    figmaLink:
      'https://www.figma.com/file/vANdzsGpqlhXhlFdHAJioP/IRIS-Master?node-id=2001%3A6424&t=Hep59MU6vloKSJeI-1',
    images: [
      { jpg: iris1, webp: iris1w, alt: 'A landing page using Iris.' },
      { jpg: iris2, webp: iris2, alt: `Iris' illustrations.`, main: true },
      { jpg: iris3, webp: iris3, alt: `Iris' components.` },
    ],
    description: `A design system I designed for My Store, Canasta Rosa's sibling product.`,
  },
  {
    projectTitle: 'RX Watches',
    year: 2019,
    link: '/projects/RXWatches',
    gitLink: 'https://gitlab.com/VirtualClover/RXWatches',
    readingTime: 1,
    images: [
      { jpg: rx1, webp: rx1w, alt: `RX'S COVER.`, main: true },
      { jpg: rx2, webp: rx2w, alt: `RX WATCHES.` },
      { jpg: rx3, webp: rx3w, alt: `RX'S ICONS.` },
    ],
    description: 'RX is a demo of a landing page about watches.',
  },
  {
    projectTitle: 'Illustrations',
    year: 2019,
    link: 'https://virtualclover.tumblr.com/',
    images: [
      { jpg: illusts1, webp: illusts1w, alt: `Illustration 1`, main: true },
      { jpg: illusts2, webp: illusts2w, alt: `Illustration 2` },
      { jpg: illusts3, webp: illusts3w, alt: `Illustration 3` },
    ],
    description: 'I also do doodles from time to time.',
  },
];

export const jobsItems: Array<Job> = [
  {
    title: 'Product Designer',
    company: { name: 'Bitso', link: '' },
    year: 2021,
    image: { jpg: bitso, webp: bitsow, alt: `Bitso's logo` },
  },
  {
    title: 'UX/UI Designer',
    company: { name: 'Canasta Rosa', link: '' },
    year: 2019,
    image: { jpg: canasta, webp: canastaw, alt: `Canasta Rosa's logo` },
  },
  {
    title: 'Web Design intern',
    company: { name: 'Secoco Martz', link: '' },
    year: 2017,
    image: { jpg: secoco, webp: secocow, alt: `Secoco Martz's logo` },
  },
];

export const personalData = {
  name: 'Guillermo López',
  role: 'Prdouct designer / Frontend engineer',
  email: 'memo@virtualclover.ltd',
  alias: 'VIRTUAL CLOVER',
};

export const companies = {
  amazon: {
    name: 'Amazon',
    image: amazonImage,
  },
  discord: {
    name: 'Discord',
    image: discordImage,
  },
  duckDuckGo: {
    name: 'Duck Duck Go',
    image: duckduckgoImage,
  },
  gitlab: {
    name: 'Gitlab',
    image: gitlabImage,
  },
  gbm: {
    name: 'GBM',
    image: gbmImage,
  },
  google: {
    name: 'Google',
    image: googleImage,
  },
  ibm: {
    name: 'IBM',
    image: ibmImage,
  },
  mercadoLibre: {
    name: 'Mercado Libre',
    image: mercadoLibreImage,
  },
  meta: {
    name: 'Meta',
    image: metaImage,
  },
  paypal: {
    name: 'Paypal',
    image: paypalImage,
  },
  spotify: {
    name: 'Spotify',
    image: spotifyImage,
  },
  uber: {
    name: 'Uber',
    image: uberImage,
  },
  microsoft: {
    name: 'Microsoft',
    image: microsoftImage,
  },
};
