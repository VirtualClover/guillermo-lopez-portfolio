import { FunctionComponent, SVGAttributes } from 'react';

export interface ImageProps {
  jpg: string | FunctionComponent<SVGAttributes<SVGElement>>;
  webp: string | FunctionComponent<SVGAttributes<SVGElement>>;
  alt: string;
  main?: boolean;
}

export interface Project {
  projectTitle: string;
  year: number;
  link: string;
  figmaLink?: string;
  gitLink?: string;
  images: Array<ImageProps>;
  description: string;
  readingTime?: number;
}

export interface Company {
  name: string;
  link: string;
}

export interface Article {
  readingTime?: number;
}

export interface Job {
  title: string;
  company: Company;
  year: number;
  image: ImageProps;
}

export interface Referral {
  name: string;
  image: string;
}

export interface VideoProps {
  loop?: boolean;
  mp4: string;
  webm: string;
  alt: string;
  preload?: 'none' | 'auto' | 'metadata';
}

export interface IFrameProps {
  src: string;
  alt: string;
}
