declare module '*.jpg';
declare module '*.png';
declare module '*.jpeg';
declare module '*.webp' {
  const value: string;
  export default value;
}
declare module "*.svg" {
  const content: React.FunctionComponent<React.SVGAttributes<SVGElement>>;
  export default content;
}
declare module '*.gif';
declare module '*.mp4' {
  const src: string;
  export default src;
}

declare module '*.webm' {
  const src: string;
  export default src;
}
declare module '*.scss' {
  const content: { [className: string]: string };
  export = content;
}
declare module '*.mdx' {
  let MDXComponent: (props: any) => JSX.Element;
  export default MDXComponent;
}
