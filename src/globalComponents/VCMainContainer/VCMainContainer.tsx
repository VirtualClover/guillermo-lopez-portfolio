import '../../utils/theme/globalTheme.scss';

import * as styles from './VCMainContainer.module.scss';

import React, { useEffect, useState } from 'react';
import { companies, personalData } from '../../utils/data/globalData';
import {
  faArrowLeft,
  faArrowRight,
  faBriefcase,
  faClose,
  faEnvelope,
  faFileSignature,
  faGlasses,
  faPaintBrush,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

import { ComponentProps } from './VCMainContainer.model';
import Helmet from 'react-helmet';
import { VCFooter } from '../VCFooter';
import { VCLink } from '../VCLink';
import { VCNavBar } from '../VCNavBar';
import { VCSurvey } from '../VCSurvey';
import { fab } from '@fortawesome/free-brands-svg-icons';
//Icons
import { library } from '@fortawesome/fontawesome-svg-core';

library.add(
  fab,
  faGlasses,
  faEnvelope,
  faArrowRight,
  faArrowLeft,
  faPaintBrush,
  faUser,
  faBriefcase,
  faFileSignature,
  faClose
);

/**
 * The main container for pages
 * @param param0
 * @returns
 */
export const VCMainContainer = ({
  meta = {
    title: 'Guillermo Lopez',
    description: 'This is my protfolio site.',
    ogType: 'website',
    ogImage: '',
    article: {
      readingTime: 1,
    },
  },
  navConfig = { buttons: [], title: 'always' },
  children,
  footerTouchPoints = true,
}: ComponentProps): JSX.Element => {
  const [surveyButton, setSurveyButton] = useState(<></>);
  const [surveyComponent, setsurveyComponent] = useState(<></>);
  const urlParams = new URLSearchParams(
    typeof window !== 'undefined' ? window.location.search : ''
  );
  const referral = typeof window !== 'undefined' ? urlParams.get('ref') : '';
  useEffect(() => {
    if (referral) {
      if (companies[referral]) {
        setSurveyButton(
          <li>
            <VCLink
              link={'https://4kxom3mw1mf.typeform.com/to/SF68Iv37'}
              icon={'file-signature'}
              label={'Answer feedback form!'}
            />
          </li>
        );
        const closedCookie = document.cookie
          .split('; ')
          .find((row) => row.startsWith('closedRef='))
          ?.split('=')[1];
        if (!closedCookie) {
          setsurveyComponent(<VCSurvey referral={referral} />);
          document.cookie = `ref=true`;
        }
      }
    } else {
      const refCookie = document.cookie
        .split('; ')
        .find((row) => row.startsWith('ref='))
        ?.split('=')[1];
      if (refCookie) {
        setSurveyButton(
          <li>
            <VCLink
              link={'https://4kxom3mw1mf.typeform.com/to/SF68Iv37'}
              icon={'file-signature'}
              label={'Answer the feedback form!'}
            />
          </li>
        );
      }
    }
  }, []);
  return (
    <main className={styles.mainWrapper}>
      <Helmet
        htmlAttributes={{
          lang: 'en',
          prefix: 'og: https://ogp.me/ns#',
        }}
      >
        <title>{meta.title}</title>
        <meta
          name="author"
          content={`${personalData.name}, ${personalData.email}`}
        />
        <meta
          name="designer"
          content={`${personalData.name}, ${personalData.email}`}
        />
        <meta name="og:email" content={personalData.email} />
        <meta property="og:title" content={meta.title} />
        <meta
          property="og:description"
          content={(meta.article ? 'A case study for ' : '') + meta.description}
        />
        <meta name="owner" content={personalData.name} />
        <meta
          property="og:image"
          content={`https://virtualclover.com.mx${meta.ogImage}`}
        />
        <meta name="twitter:card" content={`summary_large_image`} />
        <meta property="og:type" content={meta.ogType} />
        <meta
          name="description"
          content={(meta.article ? 'A case study for ' : '') + meta.description}
        ></meta>
      </Helmet>
      {meta.article ? (
        <Helmet>
          <meta name="twitter:label1" content="Written by" />
          <meta name="twitter:data1" content={personalData.name} />
          <meta name="twitter:label2" content="Reading time" />
          <meta property="article:section" content="Technology" />
          <meta
            name="twitter:data2"
            content={`${meta.article.readingTime} minute(s)`}
          />
        </Helmet>
      ) : (
        ''
      )}
      {surveyComponent}
      <VCNavBar title={navConfig.title}>
        <ul>
          {navConfig.buttons.map(function (button, i) {
            return (
              <li key={i}>
                <VCLink
                  link={button.link}
                  icon={button.icon}
                  label={button.label}
                />
              </li>
            );
          })}
          {surveyButton}
        </ul>
      </VCNavBar>
      <div>{children}</div>
      <VCFooter touchpoints={footerTouchPoints} />
    </main>
  );
};
