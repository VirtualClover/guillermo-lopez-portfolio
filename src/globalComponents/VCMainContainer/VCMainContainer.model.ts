import { Article } from '../../utils/types/globalTypes';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

interface Meta {
  title: string;
  description: string;
  ogImage: string;
  ogType: string;
  article?: Article;
}

interface Button {
  link: string;
  icon: IconProp;
  label: string;
}

interface NavConfig {
  buttons: Array<Button>;
  title?: 'fadeIn' | 'none' | 'always';
}

export interface ComponentProps {
  meta: Meta;
  navConfig: NavConfig;
  children: JSX.Element | JSX.Element[];
  footerTouchPoints?: boolean;
}
