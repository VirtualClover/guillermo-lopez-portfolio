import '../../utils/theme/globalTheme.scss';

import * as styles from './VCSurvey.module.scss';

import React, { useEffect, useState } from 'react';

import { ComponentProps } from './VCSurvey.model';
import { VCButton } from '../VCButton';
import { companies } from '../../utils/data/globalData';
//Icons
import { motion } from 'framer-motion';

/**
 * The main container for pages
 * @param param0
 * @returns
 */
export const VCSurvey = ({
  referral = 'amazon',
}: ComponentProps): JSX.Element => {
  const [isVisible, setIsVisible] = useState(true);

  return (
    <motion.aside
      initial={{
        y: -20,
        opacity: 0,
      }}
      animate={{ y: 0, opacity: 1 }}
      transition={{
        type: 'spring',
        mass: 0.9,
        delay: 2,
        stiffness: 75,
        duration: 0.4,
      }}
      className={`${styles.mainWrapper} ${isVisible ? '' : styles.notVisible}`}
      aria-hidden={!isVisible}
      {...(isVisible ? '' : { tabIndex: -1 })}
    >
      <img src={companies[referral].image} alt={'Company logo'} aria-hidden />
      <div>
        <p>
          <span aria-label={'A special message:'} />
          Hi <b>{companies[referral].name} team! </b> <br />
          <small>
            {' '}
            (Or recruiting team on their behalf) 👋
            <br />
            After you're done exploring, would you mind clicking on this button
            ☝ and filling a quick feedback form?
          </small>
        </p>
      </div>
      <div className={styles.arrowUp} />
      <VCButton
        label={'Close the message.'}
        icon={'close'}
        link={''}
        onClick={() => {
          setIsVisible(false);
          document.cookie = `closedRef=true`;
        }}
      />
    </motion.aside>
  );
};
