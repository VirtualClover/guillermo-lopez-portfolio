import * as styles from './VCTimeline.module.scss';

import React from 'react';
import { VCJobPin } from '../VCJobPin/VCJobPin';
import bitso from '../../images/resume/bitso.jpg';
import bitsow from '../../images/resume/bitso.webp';
import canasta from '../../images/resume/canasta.jpg';
import canastaw from '../../images/resume/canasta.webp';
import secoco from '../../images/resume/secoco.jpg';
import secocow from '../../images/resume/secoco.webp';

/**
 * The basic component used for images
 */
export const VCTimeline = () => {
  return (
    <div className={styles.mainWrapper}>
      <h2 aria-label={`Where I've been at?`}></h2>
      <ul>
        <VCJobPin
          delay={0}
          year={2021}
          image={{ jpg: bitso, webp: bitsow, alt: `Bitso` }}
          title={'Product Designer'}
          company={{
            name: 'Bitso',
            link: 'https://www.linkedin.com/company/3635123',
          }}
        >
          At Bitso I'm helping create scalable and unified products through our{' '}
          <b>Design System</b>.
        </VCJobPin>
        <VCJobPin
          delay={1}
          year={2019}
          image={{
            jpg: canasta,
            webp: canastaw,
            alt: `Canasta Rosa's`,
          }}
          title={'UX/UI Designer'}
          company={{
            name: 'Canasta Rosa',
            link: 'https://www.linkedin.com/company/33213955',
          }}
        >
          Designed and helped develop <b>minor features to entire products</b>{' '}
          and created their Design System.
        </VCJobPin>
        <VCJobPin
          delay={2}
          year={2017}
          image={{
            jpg: secoco,
            webp: secocow,
            alt: `Secoco Martz`,
          }}
          title={'Web Design intern'}
          company={{
            name: 'Secoco Martz',
            link: 'https://www.linkedin.com/company/3329318',
          }}
        >
          Developed & design their sites in vanilla <b>HTML, CSS and JS</b> for each branch of the company.
        </VCJobPin>
      </ul>
    </div>
  );
};
