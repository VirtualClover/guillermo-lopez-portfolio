import { ImageProps } from "../../utils/types/globalTypes";

export interface ComponentProps extends ImageProps {
  className?: string;
  loading?: 'lazy' | 'eager';
}
