import * as styles from './VCImg.module.scss';

import { ComponentProps } from './VCImg.model';
import React from 'react';

/**
 * The basic component used for images
 */
export const VCImg = ({
  className,
  webp,
  jpg,
  alt = '',
  loading = 'eager',
}: ComponentProps) => {
  return (
    <div className={`${styles.mainWrapper} ${className}`}>
      <img
        srcSet={`${webp} 1x, ${jpg} 1x`}
        alt={`An image depicting: ${alt}.`}
        loading={loading}
      />
      <figcaption aria-hidden>{alt}</figcaption>
    </div>
  );
};
