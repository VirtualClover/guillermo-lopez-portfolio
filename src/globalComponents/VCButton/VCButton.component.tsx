import * as styles from './VCButton.module.scss';

import { ComponentProps } from './VCButton.model';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

/**
 * The basic button component used for href.
 * @param {*} props
 * @returns A button component
 */
export const VCButton = ({
  className,
  icon = ['fab', 'figma'],
  onClick = () => {},
  label,
}: ComponentProps) => {
  return (
    <>
      <button
        className={`${styles.mainWrapper} vc-txts-link ${className}`}
        onClick={onClick}
        title={label}
        aria-label={label}
      >
        <FontAwesomeIcon icon={icon} />
      </button>
    </>
  );
};
