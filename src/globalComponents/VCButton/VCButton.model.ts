import React, { MouseEventHandler } from 'react';

import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface ComponentProps {
  className?: string;
  icon: IconProp;
  link: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
  label: string;
}
