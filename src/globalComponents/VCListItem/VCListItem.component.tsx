import * as styles from './VCListItem.module.scss';

import { ComponentProps } from './VCListItem.model';
import { Link } from 'gatsby';
import React from 'react';
import { VCImg } from '../VCImg';
import { VCLink } from '../VCLink';

// markup
export const VCListItem = ({
  projectTitle = 'PROJECT_TITLE',
  link = '/',
  images = [],
  description = 'DESC',
  figmaLink,
  gitLink,
  total = 0,
  currentNumber = 0,
  readingTime = 3,
}: ComponentProps) => {
  const isExternal = link.match(/^\//gi) ? false : true;
  return (
    <>
      {isExternal ? (
        <div className={styles.itemWrapper}>
          <a
            href={`${link}`}
            target={'_blank'}
            aria-label={`${projectTitle}. ${description}. Project ${currentNumber} of ${total}.`}
          />
          <div className={styles.contentWrapper} aria-hidden>
            <div>
              <h3 className={styles.projectTitle}>
                {projectTitle}
                <span>.</span>
              </h3>
              <p>{description}</p>
            </div>
            <p>
              <span aria-label={`Project ${currentNumber} of ${total}.`}>
                {currentNumber} of {total}
              </span>
            </p>
          </div>
          <ul>
            {figmaLink && (
              <li>
                <VCLink
                  label="Go to the Figma file."
                  icon={['fab', 'figma']}
                  link={figmaLink}
                />
              </li>
            )}
            {gitLink && (
              <li>
                {' '}
                <VCLink
                  label="Go to the Git repository."
                  icon={['fab', 'github']}
                  link={gitLink}
                />
              </li>
            )}
          </ul>
          <div className={styles.imgWrapper} aria-hidden>
            {images.map(function (item, i) {
              return (
                <VCImg
                  key={i}
                  webp={item.webp}
                  jpg={item.jpg}
                  alt={item.alt}
                  className={!item.main ? styles.notMainImg : ''}
                  loading={currentNumber > 2 ? 'lazy' : 'eager'}
                />
              );
            })}
          </div>
        </div>
      ) : (
        <div className={styles.itemWrapper}>
          <Link
            to={`${link}`}
            title={`Reading Time: ${readingTime} minute(s).`}
            aria-label={`${projectTitle}. ${description}. Listening Time: ${readingTime} minutes. Project ${currentNumber} of ${total}.`}
          />
          <div className={styles.contentWrapper} aria-hidden>
            <div>
              <h3 className={styles.projectTitle}>
                {projectTitle}
                <span>.</span>
              </h3>
              <p>
                {description}
                <span />
              </p>
            </div>
            <p>
              <span>
                {currentNumber} of {total}
              </span>
            </p>
          </div>
          <ul>
            {figmaLink && (
              <li>
                <VCLink
                  label="Go to the Figma file."
                  icon={['fab', 'figma']}
                  link={figmaLink}
                />
              </li>
            )}
            {gitLink && (
              <li>
                {' '}
                <VCLink
                  label="Go to the Git repository."
                  icon={['fab', 'github']}
                  link={gitLink}
                />
              </li>
            )}
          </ul>
          <div className={styles.imgWrapper} aria-hidden>
            {images.map(function (item, i) {
              return (
                <VCImg
                  key={i}
                  webp={item.webp}
                  jpg={item.jpg}
                  alt={item.alt}
                  className={!item.main ? styles.notMainImg : ''}
                  loading={currentNumber > 2 ? 'lazy' : 'eager'}
                />
              );
            })}
          </div>
        </div>
      )}
    </>
  );
};
