import { ImageProps, Project } from '../../utils/types/globalTypes';

export interface ComponentProps extends Project {
  total: number;
  currentNumber: number;
}
