import { VideoProps } from '../../utils/types/globalTypes';

export interface ComponentProps extends VideoProps {
  className?: string;
}
