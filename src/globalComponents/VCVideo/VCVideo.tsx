import * as styles from './VCVideo.module.scss';

import { ComponentProps } from './VCVideo.model';
import React from 'react';

/**
 * The basic component used for images
 */
export const VCVideo = ({
  className,
  webm,
  mp4,
  loop = false,
  alt = 'An image',
  preload = 'metadata',
}: ComponentProps) => {
  return (
    <div className={`${styles.mainWrapper} ${className}`}>
      <video
        playsInline
        controls={!loop}
        muted={loop}
        loop={loop}
        autoPlay={loop}
        preload={preload}
      >
        <source src={webm} type="video/webm" />
        <source src={mp4} type="video/mp4" />
      </video>
      <figcaption aria-label={`Video depicting: ${alt}.`}>{alt}</figcaption>
    </div>
  );
};
