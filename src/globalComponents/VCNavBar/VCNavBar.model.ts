import { ReactNode } from "react";

export interface ComponentProps {
  title?: 'fadeIn' | 'none' | 'always'; 
  children: ReactNode;
}
