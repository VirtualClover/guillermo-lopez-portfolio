import * as styles from './VCNavBar.module.scss';

import React, { useEffect, useState } from 'react';

import { ComponentProps } from './VCNavBar.model';
import { Link } from 'gatsby';
import { VCLink } from '../VCLink';
import { personalData } from '../../utils/data/globalData';

const HomeConfig = () => (
  <ul>
    <li>
      <VCLink link="/about-me" icon="user" label="About me" />
    </li>
  </ul>
);

const GeneralConfig = () => (
  <ul>
    <li>
      <VCLink link="/" icon="briefcase" label="Go to my portfolio" />
    </li>
    <li>
      <VCLink link="/about-me" icon="user" label="About me" />
    </li>
  </ul>
);

/**
 * The navigation bar component
 */
export const VCNavBar = ({ title = 'always', children }: ComponentProps) => {
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);
    // clean up code
    window.removeEventListener('scroll', onScroll);
    window.addEventListener('scroll', onScroll, { passive: true });
    return () => window.removeEventListener('scroll', onScroll);
  }, []);

  return (
    <nav className={`${styles.mainWrapper}`}>
      <div aria-hidden>
        <Link
          aria-hidden
          tabIndex={-1}
          to="/"
          className={
            (offset && title == 'fadeIn') || title == 'always'
              ? styles.shown
              : ''
          }
        >
          <span>{personalData.name}</span>
        </Link>
      </div>
      {children}
      {/*<div aria-hidden className={styles.gradient}/>*/}
    </nav>
  );
};
