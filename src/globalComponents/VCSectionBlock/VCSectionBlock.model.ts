import { IFrameProps, ImageProps, VideoProps } from '../../utils/types/globalTypes';

import React from 'react';

export interface ComponentProps {
  className?: string;
  children: React.ReactNode;
  image?: ImageProps;
  iFrame?: IFrameProps;
  video?: VideoProps;
  title: string;
  sectionNumber: number;
  sectionTotal: number;
}
