import * as styles from './VCSectionBlock.module.scss';

import { ComponentProps } from './VCSectionBlock.model';
import React from 'react';
import { VCIFrame } from '../VCIFrame';
import { VCImg } from '../VCImg';
import { VCVideo } from '../VCVideo';

/**
 * The section block for each case
 */
export const VCSectionBlock = ({
  className,
  children,
  image,
  video,
  iFrame,
  title,
  sectionNumber,
  sectionTotal,
}: ComponentProps) => {
  return (
    <section className={`${styles.mainWrapper} ${className}`}>
      <div>
        <p>
          <span aria-label={`Section ${sectionNumber} of ${sectionTotal}.`}>
            {sectionNumber} of {sectionTotal}
          </span>
        </p>
        <h2 className="vc-txts-subtitle2" aria-label={`${title}.`}>
          {title}
        </h2>
        {children}
      </div>
      {image && (
        <VCImg
          jpg={image.jpg}
          webp={image.webp}
          alt={image.alt}
          loading={sectionNumber > 1 ? 'lazy' : 'eager'}
        />
      )}
      {video && (
        <VCVideo
          webm={video.webm}
          mp4={video.mp4}
          alt={video.alt}
          loop={video.loop}
        />
      )}
      {iFrame && <VCIFrame src={iFrame.src} alt={iFrame.alt} />}
    </section>
  );
};
