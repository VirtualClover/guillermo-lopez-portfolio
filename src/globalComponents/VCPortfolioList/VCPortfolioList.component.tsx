import * as styles from './VCPortfolioList.module.scss';

import React from 'react';
import { VCListItem } from '../VCListItem';
import { listItems } from '../../utils/data/globalData';
import { motion } from 'framer-motion';

/**
 * The portfolio list
 * @returns
 */
export const VCPortfolioList = () => {
  return (
    <div className={styles.mainWrapper}>
      <motion.h2
        initial={{ x: -500 }}
        animate={{ x: 0 }}
        exit={{ x: -500 }}
        transition={{
          type: 'spring',
          mass: 0.35,
          delay: 0.2,
          stiffness: 75,
          duration: 0.4,
        }}
        aria-label={`The works.`}
      >
        The works
      </motion.h2>
      <motion.ul
        initial={{ y: 1200, opacity: 0 }}
        animate={{ y: 0, opacity: 1 }}
        exit={{ y: 0, opacity: 0 }}
        transition={{
          type: 'spring',
          mass: 0.4,
          delay: 0.3,
          duration: 0.5,
        }}
      >
        {listItems.map(function (item, i) {
          return (
            <li key={i}>
              <VCListItem
                currentNumber={i + 1}
                total={listItems.length}
                projectTitle={item.projectTitle}
                year={item.year}
                link={item.link}
                images={item.images}
                description={item.description}
                gitLink={item.gitLink}
                figmaLink={item.figmaLink}
                readingTime={item.readingTime}
              />
            </li>
          );
        })}
      </motion.ul>
    </div>
  );
};
