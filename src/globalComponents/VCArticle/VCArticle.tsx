import * as styles from './VCArticle.module.scss';

import {
  faArrowLeft,
  faArrowRight,
  faBriefcase,
  faEnvelope,
  faGlasses,
  faPaintBrush,
  faPalette,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

import { ComponentProps } from './VCArticle.model';
import React from 'react';
import { VCLink } from '../VCLink';
import { VCMainContainer } from '../VCMainContainer';
//model
import { fab } from '@fortawesome/free-brands-svg-icons';
//Icons
import { library } from '@fortawesome/fontawesome-svg-core';
import { motion } from 'framer-motion';
import { personalData } from '../../utils/data/globalData';

library.add(
  fab,
  faGlasses,
  faEnvelope,
  faArrowRight,
  faArrowLeft,
  faPalette,
  faUser,
  faBriefcase,
  faPaintBrush
);
// markup
export const VCArticle = ({
  metaData = {
    projectTitle: 'Project title',
    description: 'A project',
    images: [],
    year: 2019,
    link: '/',
    figmaLink: '',
    gitLink: '',
    readingTime: 1,
  },
  children,
  ogImage,
}: ComponentProps) => {
  return (
    <VCMainContainer
      meta={{
        title: `${metaData.projectTitle} | ${personalData.name}`,
        description: metaData.description,
        ogType: 'article',
        ogImage: ogImage,
        article: {
          readingTime: metaData.readingTime,
        },
      }}
      navConfig={{
        buttons: [
          { link: '/', icon: 'briefcase', label: 'Go to my portfolio' },
          { link: '/about-me', icon: 'user', label: 'About me' },
        ],
      }}
    >
      <article className={styles.mainWrapper}>
        <motion.header
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{
            type: 'spring',
            mass: 0.35,
            stiffness: 75,
            duration: 0.4,
          }}
        >
          {' '}
          <div className={styles.introParagraph}>
            <div>
              <h1
                className={styles.projectTitle}
                aria-label={`${metaData.projectTitle}.`}
              >
                {metaData.projectTitle}
              </h1>

              <p>
                {metaData.description} <br />{' '}
              </p>
            </div>
            <p>
              {' '}
              <motion.span
                exit={{ opacity: 0 }}
                aria-label={`Listening time: ${metaData.readingTime} minute(s).`}
              >
                Reading time: {metaData.readingTime} minute(s).
              </motion.span>
            </p>
          </div>
          <ul>
            {metaData.figmaLink && (
              <li>
                <VCLink
                  label="Go to the Figma file."
                  icon={['fab', 'figma']}
                  link={metaData.figmaLink}
                />
              </li>
            )}
            {metaData.gitLink && (
              <li>
                <VCLink
                  label="Go to the Git repository."
                  icon={['fab', 'github']}
                  link={metaData.gitLink}
                />
              </li>
            )}
          </ul>
        </motion.header>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{
            type: 'spring',
            delay: 0.1,
            duration: 0.5,
          }}
        >
          {children}
        </motion.div>
      </article>
    </VCMainContainer>
  );
};
