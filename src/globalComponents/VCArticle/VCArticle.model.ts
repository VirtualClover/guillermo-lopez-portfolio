import { Project } from '../../utils/types/globalTypes';
import React from 'react';

export interface ComponentProps {
  ogImage: string;
  metaData: Project;
  children?: React.ReactNode;
}