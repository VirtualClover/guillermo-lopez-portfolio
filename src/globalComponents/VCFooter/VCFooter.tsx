import * as styles from './VCFooter.module.scss';

import { ComponentProps } from './VCFooter.model';
import React from 'react';
import { VCLink } from '../VCLink';

/**
 * The footer component
 */
export const VCFooter = ({ touchpoints = true }: ComponentProps) => {
  return (
    <footer className={styles.mainWrapper}>
      <p>
        Site designed and{' '}
        <a
          href="https://gitlab.com/VirtualClover/guillermo-lopez-portfolio"
          target={'_blank'}
        >
          coded
        </a>{' '}
        by yours truly. ❤
      </p>
      {touchpoints && (
        <ul>
          <li>
            <VCLink
              icon={'envelope'}
              label={'Send me a mail!'}
              link={'mailto:memo@virtualclover.ltd'}
            />
          </li>
          <li>
            <VCLink
              icon={['fab', 'github']}
              label={'Check my git repos.'}
              link={'https://gitlab.com/VirtualClover'}
            />
          </li>
          <li>
            <VCLink
              icon={['fab', 'linkedin']}
              label={'Go to my linkedIn profile.'}
              link={'https://www.linkedin.com/in/vc-memo-lopez/'}
            />
          </li>
          <li>
            <VCLink
              icon={'paint-brush'}
              label={'Got to my illustration portfolio.'}
              link={'https://virtualclover.tumblr.com/'}
            />
          </li>
        </ul>
      )}
    </footer>
  );
};
