import { IFrameProps } from "../../utils/types/globalTypes";

export interface ComponentProps extends IFrameProps {
  className?: string;
}
