import * as styles from './VCIFrame.module.scss';

import { ComponentProps } from './VCIFrame.model';
import React from 'react';

/**
 * The basic component for iFrames
 */
export const VCIFrame = ({
  className,
  src,
  alt = 'An iframe',
}: ComponentProps) => {
  return (
    <div className={`${styles.mainWrapper} ${className}`}>
      <iframe
        width="100%"
        height={'100%'}
        src={src}
        allowFullScreen
        title={`Figma file depicting: ${alt}.`}
        loading={'lazy'}
      />
      <figcaption aria-hidden>{alt}</figcaption>
    </div>
  );
};
