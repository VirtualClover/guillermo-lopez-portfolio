import { IconProp } from '@fortawesome/fontawesome-svg-core';
import React from 'react';

export interface ComponentProps {
  className?: string;
  icon: IconProp;
  link: string;
  onClick?: React.MouseEventHandler<HTMLAnchorElement>;
  label: string;
}
