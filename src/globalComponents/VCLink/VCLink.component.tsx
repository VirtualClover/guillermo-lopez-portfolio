import * as styles from './VCLink.module.scss';

import { ComponentProps } from './VCLink.model';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'gatsby';
import React from 'react';

/**
 * The basic button component used for href.
 * @param {*} props
 * @returns A button component
 */
export const VCLink = ({
  className,
  icon = ['fab', 'figma'],
  link,
  onClick = () => {},
  label,
}: ComponentProps) => {
  const isExternal = link.match(/^\//gi) ? false : true;

  return (
    <>
      {isExternal ? (
        <a
          className={`${styles.mainWrapper} vc-txts-link ${className}`}
          href={link}
          title={label}
          target={'_blank'}
          aria-label={label}
        >
          <FontAwesomeIcon icon={icon} />
        </a>
      ) : (
        <Link
          className={`${styles.mainWrapper} vc-txts-link ${className}`}
          to={link}
          title={label}
          aria-label={label}
        >
          <FontAwesomeIcon icon={icon} />
        </Link>
      )}
    </>
  );
};
