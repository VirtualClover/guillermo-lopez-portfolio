import * as styles from './VCJobPin.module.scss';

import { ComponentProps } from './VCJobPin.model';
import React from 'react';
import { motion } from 'framer-motion';

/**
 * Used for displaying jobs in the timeline
 */
export const VCJobPin = ({
  image,
  company,
  title,
  children,
  year,
  delay = 0,
}: ComponentProps) => {
  return (
    <motion.li
      initial={{
        y: 300,
        opacity: 0,
      }}
      animate={{ y: 0, opacity: 1 }}
      transition={{
        type: 'spring',
        mass: 0.4,
        delay: 0.5 + 0.02 * delay,
        stiffness: 75,
        duration: 0.4,
      }}
      className={`${styles.mainWrapper}`}
    >
      <div aria-label={`Year: ${year}.`} />
      <img
        srcSet={`${image.webp} 1x, ${image.jpg} 1x`}
        alt={`The logo of ${image.alt}.`}
        aria-hidden
        width={40}
        height={40}
      />
      <h2 aria-label={`Role: ${title}.`}>{title}</h2>
      <a
        href={company.link}
        target={'_blank'}
        aria-label={`Company: ${company.name}.`}
      >
        {company.name}
      </a>
      <p className={styles.description}>{children}</p>
      <p className={styles.yearWrapper} aria-hidden>
        <span>{year}</span>
      </p>
      <div className={styles.pin} aria-hidden>
        <div />
      </div>
    </motion.li>
  );
};
