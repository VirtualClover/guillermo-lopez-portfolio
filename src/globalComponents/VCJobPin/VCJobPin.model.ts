import { ImageProps, Job } from '../../utils/types/globalTypes';

import { ReactNode } from 'react';

export interface ComponentProps extends Job {
  children: ReactNode;
  delay: number;
}
